/*		appelant.c			*/

#include  <stdio.h>
#include  <dlfcn.h>

void affichage(char *titre, float vals[4]);

int main(int argc, char *argv[])
{
void		*handle;
float		values[4];

void (*funcname)(const char*, const float *);

handle = dlopen("./plugiciel.so", RTLD_LAZY);
if (!handle) {
        /* fail to load the library */
        fprintf(stderr, "Error: %s\n", dlerror());
        return 1;
        }

*(void**)(&funcname) = dlsym(handle, "affichage");

values[0] = 13.37;	values[1] = 16.64;
values[2] = 3.14159;	values[3] = 0.5;
funcname("rgb * a =", values);

dlclose(handle);

return 0;
}
