#!/usr/bin/perl  -w

use strict;
use GDBM_File;
my $nomdb = "exemple.gdbm";
my (%DB, @champs);
my ($user, $gecos);
open(SOURCE, "< /etc/passwd") or die "source $!";
tie(%DB, "GDBM_File", $nomdb, GDBM_WRCREAT, 0666) or die "gdbm $!";

while (<SOURCE>) {
    @champs = split ":", $_;
    $user = $champs[0]."\0";
    $gecos = $champs[4]."\0";
    $DB{$user} = $gecos;
    }

untie %DB;
close SOURCE;
0;
