/*
 *	no-op.c is an useless shell filter
 */
#include  <stdio.h>
#include  <ctype.h>

int main(int argc, char *argv[])
{
int	quux, baz;

while (EOF != (quux=getchar())) {
	baz = toupper(quux);
	if ('O'==baz || 'P'==baz) continue;
	putchar(quux);
	}

return 0;
}
