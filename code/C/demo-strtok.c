/*
 *		+-------------+
 *		| demo strtok |
 *		+-------------+
 */

#include   <stdio.h>
#include   <string.h>
#include   <ctype.h>
/*-------------------------------------------------------*/
void dump_line(char *titre, char *ligne, int combien)
{
int		foo;
unsigned char	*ptr;

printf("   --- %s\n", titre);
ptr = (unsigned char *)ligne;
for (foo=0; foo<combien; foo++) {
	if (ptr[foo])	printf("%02x ", ptr[foo]);
	else		printf("\\0 ");
	}
puts("");
for (foo=0; foo<combien; foo++) {
	printf(" %c ", isprint(ptr[foo]) ? ptr[foo] : ' ');
	}
puts("");
}
/*-------------------------------------------------------*/
int main(int argc, char *argv[])
{
char		input[] = "foo   bar\tquux wiz";
char		*cptr, *copie;
int		foo;

dump_line("original", input, sizeof(input));
copie = strdup(input);
dump_line("copie", copie, sizeof(input));
puts("");

cptr = strtok(copie, " \t");
dump_line("1er strtok", copie, sizeof(input));
printf("   got [%s]\n", cptr);
puts("");

puts("   two more strtok...");
cptr = strtok(NULL, " \t");
printf("   got [%s]\n", cptr);
cptr = strtok(NULL, " \t");
printf("   got [%s]\n", cptr);
dump_line("2eme & 3me strtok", copie, sizeof(input));

foo = cptr - copie;		/* this is the hack :) */

printf("   index = %d\n", foo);
printf("   and we have [%s] :)\n", input+foo);
return 0;
}
