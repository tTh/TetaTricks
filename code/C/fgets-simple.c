/*
 *		exemple d'utilisation de 'fgets'
 */
#include  <stdio.h>
#include  <string.h>
#define		TL	12
int main(int argc, char *argv[])
{
char		buffer[TL+1];
while( fgets(buffer, TL, stdin) ) {
	printf("   %4ld      %s\n", strlen(buffer), buffer);
	}
return 0;
}
