#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
int		c, sec, nano;
struct timespec	ts;
int		slowdown = 300;	   /* set a default value */
float		delay;
/*
 *	get the desired slowdown from command-line
 */
if (2 == argc) {
	if (1 != sscanf(argv[1], "%d", &slowdown)) {
		fprintf(stderr, "%s: bad arg, sorry\n",
				  argv[0]);
		exit(1);
		}
	if (slowdown < 1) {
		fprintf(stderr, "%d is invalid\n",
                                  slowdown);
		exit(1);
		}
	}

/* convert the slowdown value to delay */
delay = 1.0 / (float)slowdown;
sec  = (int)delay;
nano = (int)(1000*1000*1000*(delay - (int)delay));

ts.tv_sec  = sec;
ts.tv_nsec = nano;
while ( EOF != (c=getchar()) )
	{
	putchar(c), fflush(stdout);
	nanosleep(&ts, NULL);
	}

return 0;
}
