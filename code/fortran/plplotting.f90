program plplotting
    use plplot
    implicit none

    ! Initializing the plotting package with a lot of
    ! default (and pertinent) values.
    call plinit ()

    ! Sets up plotter environment for simple graphs
    call plenv (-2.1, 2.1, -2.1, 2.1, 0, 0)

    ! Ends a plotting session, tidies up all the output
    ! files,switches interactive devices back into text
    ! mode and frees up any memory that was allocated. 
    call plend ()

end program
