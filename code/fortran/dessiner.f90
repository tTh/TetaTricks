program dessiner
    use plplot
    implicit none

    integer            :: i

    character(len=80) :: version
    call plgver(version)
    write (*,'(" ",a,a)') 'plPlot version: ', trim(version)

    !! call dessin_X11 (0.12, 1.51, 11)
    call dessin_dans_un_fichier ()

contains                ! -----------------------------
!------------------------------------------------------
!-    __  __               _
!-    \ \/ /   __      __ (_)  _ __
!-     \  /    \ \ /\ / / | | | '_ \
!-     /  \     \ V  V /  | | | | | |
!-    /_/\_\     \_/\_/   |_| |_| |_|
!-

subroutine dessin_X11 (sha, shb, color)
    real, intent(in)      :: sha, shb
    integer, intent(in)   :: color

    integer, parameter    :: lg = 2000
    real                  :: x(lg), y(lg)
    real                  :: k, amp
    integer               :: i

    print *, 'dessin X11:', sha, shb, color

    call plsdev('xwin')
    call plinit ()
    call plenv (-2.5, 2.5, -2.5, 2.5, 1, 1)

    amp = 2.06
    do i = 1, lg
	k = real(i)/real(lg) * 6.2832 * 2.0
        x(i) = amp * sin((k+sha))
        y(i) = amp * cos((k+shb))
    enddo
    ! print *, k, x(i), y(i)

    call plcol0 (15)         ! pure white
    call pllab  ("Fuzzfactor", "Yoyodines", "Some nice plots from tTh")
    call plcol0 (color)
    call plline (x, y)

    call plend  ()

end subroutine
!------------------------------------------------------
!-              _    __    ___     ___ 
!-       __ _  (_)  / _|  ( _ )   / _ \    __ _
!-      / _` | | | | |_   / _ \  | (_) |  / _` |
!-     | (_| | | | |  _| | (_) |  \__, | | (_| |
!-      \__, | |_| |_|    \___/     /_/   \__,_|
!-      |___/
!-

subroutine dessin_dans_un_fichier ()

    integer, parameter       :: nbpts = 179
    integer, parameter       :: nbframes = 99
    real, parameter          :: usz = 17.0             ! univers size

    real                     :: xa(nbpts), ya(nbpts)
    real                     :: xb(nbpts), yb(nbpts)
    real                     :: x(nbpts), y(nbpts)

    integer                  :: frame, i
    real                     :: coef

    character(len=89)        :: filename
    character(len=89)        :: buffer
    character(len=3)         :: str

    print *, 'Dessin dans un fichier'

    ! build the startup conditions
    do i=1, nbpts
      coef = real(i) * 0.051
      xa(i) = usz * 0.80 * sin(coef*2.0)
      ya(i) = usz * 0.70 * (-0.27 + cos(coef*3.0))
      xb(i) = usz * 0.11 * (rand() - 0.54)
      yb(i) = usz * 0.11 * (rand() - 0.97)
    enddo

    ! iterate over frames.
    do frame= 0, nbframes
      coef = real(frame) / real(nbframes)
      coef = cos(coef*3.141592654)

      write (filename, "(a, i4.4, a)") "WS/A", frame, ".png"
      ! print *, frame, "!", coef, '    =>    ', trim(filename)

      call plsdev   ('pngcairo')
      call plsfnam  (trim(filename))
      call plinit   ()
      call plenv    (-usz, usz, -usz, usz, 0, 1)
      call plcol0   (3)

      ! compute the real picture...
        do i=1, nbpts
          x(i) = xa(i)*coef - xb(i)*(1.0-coef)
          y(i) = ya(i)*coef - yb(i)*(1.0-coef)
        enddo

      ! ... and plot it *now*
      call plline (x, y)

      call plcol0 (15)         ! 15 is pure white
      call pllab("Apéro level", "Choupitude", "-- Philosophie de comptoir --")

      call plend ()
    enddo

end subroutine
!------------------------------------------------------
!-     _                    _     ____
!-    | |_   _ __    ___   (_)   |  _ \
!-    | __| | '__|  / _ \  | |   | | | |
!-    | |_  | |    | (_) | | |   | |_| |
!-     \__| |_|     \___/  |_|   |____/
!-
subroutine dessiner_en_trois_D ()

    integer, parameter       :: szgrid = 64
    integer                  :: i, j

    print *, 'Dessin en 3D (de merde :)'

    call plsdev ('xwin')
    call plinit ()

    call plend ()

end subroutine
!------------------------------------------------------
end program
