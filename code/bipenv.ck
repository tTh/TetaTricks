// Just a better beep
SinOsc s  => Envelope e   =>   dac;
// reglage de notre oscillateur
0.5         =>   s.gain;
440         =>   s.freq;
// reglage de notre enveloppe
200::ms => e.duration;
// hop !
e.keyOn();
2::second   =>   now;
e.keyOff();
1::second   =>   now;
