#include <forms.h>
/*
 *            gcc yesno.c -lforms -O YESYNO
 */
int main(int argc, char *argv[]) {
    FL_FORM *form;
    FL_OBJECT *yes,
              *no,
              *but;

    fl_initialize(&argc, argv, "Yes or No ?", 0, 0);

    form = fl_bgn_form(FL_UP_BOX, 320, 120);
    fl_add_box(FL_BORDER_BOX, 160, 40, 0, 0,      \
                            "Do you want something?");
    yes = fl_add_button(FL_NORMAL_BUTTON, \
                            40, 70, 80, 30, "Yes");
    no  = fl_add_button(FL_NORMAL_BUTTON, \
                            200, 70, 80, 30, "No");
    fl_end_form();

    fl_show_form(form, FL_PLACE_MOUSE, FL_TRANSIENT, \
                             "Vroum !");

    while (1) {
        if (fl_do_forms() == yes) {
            printf("Yes  is pushed\n");
            break;
        }
        else printf(" No  is pushed\n");
    }

    fl_finish();
    return 0;
}
