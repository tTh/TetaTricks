/*
 *	Strange Bug ?
 *
 * gcc -Wall strangebug.c -lSDL2 -o strangebug
 *
 */

#include   <stdio.h>
#include   <stdlib.h>
#include   <unistd.h>
#include   <SDL2/SDL.h>

int main(int argc, char *argv[])
{
SDL_Window	*win;
SDL_Renderer	*rend;
int		foo, idx, idy;

foo = SDL_CreateWindowAndRenderer(256, 256,
			SDL_WINDOW_SHOWN, &win, &rend);
if (foo) {
	fprintf(stderr,
		"Err %d CreateWindowAndRenderer : %s",
		foo, SDL_GetError());
        exit(1);
	}
SDL_SetRenderDrawColor(rend, 255, 0, 0, 0);
SDL_RenderClear(rend);
SDL_RenderPresent(rend);

for (idx=0; idx<256; idx++) {
	for (idy=0; idy<256; idy++) {
		SDL_SetRenderDrawColor(rend,
				127, idx, idy, 0);
		SDL_RenderDrawPoint(rend, idx, idy);
		}
	SDL_RenderPresent(rend);
	// SDL_Delay(50);	/* Is the bug here ? */
	}
SDL_RenderPresent(rend);
sleep(30);

SDL_DestroyRenderer(rend);
SDL_DestroyWindow(win);

return 0;
}