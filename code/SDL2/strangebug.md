# `strangebug`, ou le bug étrange

Pour mes premiers pas dans la sdl, j'ai voulu faire un gradient de couleur
dans une petite fenêtre, quelque chose de vraiment simple, me diriez-vous,
mais qui cache une chose étrange...

La version que j'ai utilisée pour ça (et qui est <u>très</u> ancienne) est
la 2.0.9 et j'ai beau me creuser la tête, je ne vois pas d'explication *:)*

Voici le [Code source](strangebug.c). Si vous décommentez la ligne
marquée, seul une colonne sur deux est correctement tracée,
comme le montre cette capture d'écran :

![un dégradé plein de rayures](picz/strangebug.png)

*Please explain that ...*

