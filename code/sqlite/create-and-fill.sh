#!/bin/bash

DBFILE='example.sqlite'

rm -v $DBFILE

sqlite3 $DBFILE < create-table.sql
echo 'table created'

sqlite3 $DBFILE < insert-birds.sql
echo 'bird added'

