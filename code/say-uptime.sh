#!/bin/bash

GS=" -g 9 -s 150 -a 133 "

#
#		Rendez-nous notre Mixou !
#
uptime -p						|
	sed 's/^up/My uptime is /'			|
	tee /dev/stderr					|
	espeak-ng $GS  --stdout				|
	sox - -t wav -b 16 - remix 1 1 2> /dev/null	|
	oggenc --quiet - -o -				|
	~/bin/sender > /dev/null
