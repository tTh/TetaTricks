/*	LISTEN OSC				*/

#include  <stdio.h>
#include  <unistd.h>
#include  <stdlib.h>
#include  <lo/lo.h>
#define LOCAL_PORT	"9000"

/* ------------------------------------------------------- */
void error(int num, const char *msg, const char *path)
{
fprintf(stderr, "liblo server error %d in path %s : %s\n",
					num, path, msg);
exit(1);
}
/* ------------------------------------------------------- */
int handler(const char *path, const char *types, lo_arg **argv,
                int argc, void *data, void *udata)
{
static int	event_number = 1;

printf("------  event #%d :\n", event_number++);
printf("\tpath  '%s'   types '%s'   argc %d\n",
		path, 	types, 	argc);

printf("\tdata      %p\n", data);
// if (NULL!=data) printf("\t >%s<\n", data);
printf("\tudata     %p\n", udata);
if (NULL!=udata) printf("\t\t>%s<\n", (char *)udata);
return 0;
}
/* ------------------------------------------------------- */
char signature[] = "Bourtoulots";
int main(int argc, char *argv[])
{
lo_server_thread st;
int		count = 0;

st = lo_server_thread_new(LOCAL_PORT, error);
lo_server_thread_add_method(st, "/demo", "is", handler, signature);
lo_server_thread_start(st);

for (;;) {
	fprintf(stderr, "%6d  I'm alive\n", count++);	sleep(10);
	}

return 0;
}
