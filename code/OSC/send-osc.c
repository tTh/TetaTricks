/*	SEND OSC				*/

#include  <stdio.h>
#include  <lo/lo.h>

#define REMOTE_HOST	"localhost"
#define REMOTE_PORT	"9000"

int main(int argc, char *argv[])
{
lo_address	loana;
int		foo;

fprintf(stderr, "sending to %s:%s\n", REMOTE_HOST, REMOTE_PORT);
loana = lo_address_new(REMOTE_HOST, REMOTE_PORT);
foo = lo_send(loana, "/demo", "is", 61, "meg, efface !");
fprintf(stderr, "got a %d return code ?\n", foo);
return 0;
}
