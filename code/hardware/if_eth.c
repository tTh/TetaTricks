/*
 *	code extrait des bas-fonds de l'internet obscur !
 *
 *			DON'T TRUST IT !
 *
 */

#include  <stdio.h>
#include  <unistd.h>
#include  <string.h>
#include  <stdlib.h>
#include  <sys/types.h>
#include  <sys/stat.h>
#include  <fcntl.h>
#include  <sys/ioctl.h>
#include  <net/if.h>

/* ------------------------------------------------------------- */
int get_if_flags(char *ifname, short *p_flags)
{
int			fd;
int			retcode;
struct	ifreq		req;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s \"%s\" to %p\n", __func__, ifname, p_flags);
#endif

/* Sanity check */
if ( strlen(ifname) > (IFNAMSIZ-1) )
	{
	fprintf(stderr, "name %s to long\n", ifname);
	abort();
	}

fd = socket(PF_INET, SOCK_DGRAM, 0);
if (fd < 0)
	{
	perror("socket bla...");
	return -2;
	}
#if DEBUG_LEVEL
fprintf(stderr, "fd socket bla... = %d\n", fd);
#endif

/* populate the struct for the request */
memset(&req, 0, sizeof(req));
strcpy(req.ifr_name, ifname);

/* do the call */
retcode = ioctl(fd, SIOCGIFFLAGS, &req);
if (retcode < 0)
	{
	perror("ioctl SIOCGIFFLAGS");
	close(fd);
	return -1;
	}
#if DEBUG_LEVEL
fprintf(stderr, "ioctl -> %d\n", retcode);
#endif
#if DEBUG_LEVEL
/* display the result */
fprintf(stderr, "flags = 0x%04x\n", req.ifr_flags);
#endif

close(fd);

*p_flags = req.ifr_flags;
return 0;
}
/* ------------------------------------------------------------- */
int get_if_up_or_down(char *ifname)
{
int		foo;
short		flags;

foo = get_if_flags(ifname, &flags);
#if DEBUG_LEVEL
fprintf(stderr, "get flags -> %d\n", foo);
#endif
if (foo < 0)		/* got an error ? */
	return -1;
return (flags & IFF_UP) ? 1 : 0;
}
/* ------------------------------------------------------------- */
int get_if_running(char *ifname)
{
int		foo;
short		flags;

foo = get_if_flags(ifname, &flags);
#if DEBUG_LEVEL
fprintf(stderr, "get flags -> %d\n", foo);
#endif
if (foo < 0)		/* got an error ? */
	return -1;
return (flags & IFF_RUNNING) ? 1 : 0;
}
/* ------------------------------------------------------------- */

