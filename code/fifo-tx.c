/***		named pipe --- transmiter	***/
#include  <stdio.h>
#include   <stdlib.h>
#include   <string.h>
#include  <unistd.h>
#include  <fcntl.h>
#include  "my-fifo.h"

int main(int argc, char *argv[])
{
int		fifo;
Message		message;

if (argc!=2)		exit(1);
if (-1==(fifo = open(argv[1], O_WRONLY))) {
	perror("open fifo for wr");
	exit(1);
	}
memset(&message, 0, sizeof(Message));
message.pid = getpid();
message.timestamp = dtime();
write(fifo, &message, sizeof(Message));
close(fifo);

return 0;
}