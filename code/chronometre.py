#!/usr/bin/python

from Tkinter import *
import time, math

class StopWatch(Frame):
	""" Implement a stopwatch frame widget """
	msec = 420
	def __init__(self, parent=None, **kw):
		Frame.__init__(self, parent, kw)
		self._start = 0.0
		self._elapsed = 0.0
		self._running = False
		self.timestr = StringVar()
		self._label = None
		self.makeWidgets()

	def makeWidgets(self):
		self._label = Label(self, textvariable=self.timestr,
			font = "Helvetica 36 bold",
			bg = "Grey20", fg = "Yellow",
			padx = 30, pady = 30)
		self._setTime(self._elapsed)
		self._label.pack(fill=X, expand=YES, pady=0, padx=0)

	def _update(self):
		self._elapsed = time.time() - self._start
		self._setTime(self._elapsed)
		self._timer = self.after(self.msec, self._update)

	def _setTime(self, elap):
		minutes = int(elap/60)
		seconds = int(elap - (minutes*60.0))
		fl = math.floor(elap)
		hseconds = int((elap - fl)*100)
		self.timestr.set('%03dm%02d.%02d' % \
				(minutes, seconds, hseconds))
	def Start(self):
		if not self._running:
			self._start = time.time() - self._elapsed
			self._update()
			self._running = True
	def Stop(self):
		if self._running:
			self.after_cancel(self._timer)
			self._elapsed = time.time() - self._start
			self._setTime(self._elapsed)
			self._running = False
	def Reset(self):
		""" reset the stopwath to 000m00.00 """
		self._start = time.time()
		self._elapsed = 0.0
		self._setTime(self._elapsed)
	def Dump(self):
		""" Display internals variables """
		print "start   = ", self._start
		print "elapsed = ", self._elapsed
		print "refresh = ", self.msec
		print "running = ", self._running
		# print "timestr = ", self.timestr
        def About(self):
                print "A kludge by tTh"

def build_controls(root):
	frame = Frame(root)
	sw = StopWatch(root)
	sw.pack(side=TOP)
	Button(frame, text='Start', command=sw.Start).pack(side=LEFT)
	Button(frame, text='Stop', command=sw.Stop).pack(side=LEFT)
	Button(frame, text='Reset', command=sw.Reset).pack(side=LEFT)
	Button(frame, text='Dump', command=sw.Dump).pack(side=LEFT)
	Button(frame, text='Quit', command=root.quit).pack(side=LEFT)
	frame.pack(side=BOTTOM)
	
    

def onsize(*args, **kw):
	print args[0].width, args[0].height

# --------------------------------------------------------------------

if __name__ == '__main__':
	root = Tk()
	root.title("Chronometre")
	build_controls(root)
	root.bind("<Configure>", onsize)
	root.mainloop()
