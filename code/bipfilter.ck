//--- Just a noisy beep
SqrOsc s => BPF f => Envelope e => dac;
//--- reglage de notre oscillateur
0.8         =>   s.gain;
440         =>   s.freq;
//--- reglage de notre enveloppe
100::ms     =>   e.duration;
//--- reglage du filtre passe bande
2           =>   f.Q;
1337        =>   f.freq;

fun void beep() { 
e.keyOn();          2::second   =>   now;
e.keyOff();         1::second   =>   now;
}

           () => beep;
