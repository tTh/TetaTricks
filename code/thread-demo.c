/*		thread-demo                            */

#include  <stdio.h>
#include  <unistd.h>
#include  <stdlib.h>

/* ---------------------------------------------------- */
int		compteur;          /* shared variable   */
/* ---------------------------------------------------- */
int fil(void *ptr)
{
int		foo;

for (foo=0; foo<100; foo++) {
	compteur++;
	printf("= %d\n", compteur);
	sleep(1);
	}
return 0;
}
/* --------------------------------------------------- */
int main(int argc, char *argv[])
{
int 		foo;

for (foo=0; foo<20; foo++) {
	compteur = rand()%1000;
	printf("r %d\n", compteur);
	sleep(7);
	}
return 0;
}
