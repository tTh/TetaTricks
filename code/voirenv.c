/*
 *		Low level acces to the environment space
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>

int main(int argc, char *argv[], char *envp[])
{
char	*ptr, *clone, *tok;
int	foo = 0;

while ( NULL != (ptr=envp[foo]) ) {
	printf("%4d %s\n", foo, ptr);
	clone = strdup(ptr);
	tok = strtok(clone, "=");
	printf("      key : %s\n", tok);
	tok = strtok(NULL, "=");
	printf("    value : %s\n", tok);
	free(clone);
	foo++;
	}
return 0;
}
