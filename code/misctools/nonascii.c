/*
 *		nonascii.c 
 *
 *	new Mon 03 Oct 2022 05:42:42 PM CEST
 */

#include  <stdio.h>
#include  <stdlib.h>

/* ------------------------------------------------------------------ */
int check_a_file_by_fp(FILE *fp, char *filename)
{
int		input, linenum, colnum, count;
int		flagline;

linenum = 1;		/* humans start line count at 1 */
colnum  = 1;
count   = 0;

while (EOF != (input=getc(stdin))) {
	if ('\n' == input) {
		linenum++;
		flagline = 0;
		colnum   = 1;
		}
	if ((input < 0) || (input>127)) {
		if (!flagline) {
			fprintf(stderr, "%s: nonascii 0x%x line %d, col %d\n", 
						filename,						
						input, linenum, colum);
			flagline = 1;
			}
		count++;
		}
	colnum++;
	}
return count;
}
/* ------------------------------------------------------------------ */
int check_a_file_by_name(char *name)
{
FILE		*fp;
int		foo;

if (NULL == name) {
	fp = stdin;
	}
else	{
	if (NULL==(fp=fopen(name, "r"))) {
		perror(name);
		return -1;
		}
	}

foo = check_a_file_by_fp(fp, NULL==name ? "<stdin>" : name);

return foo;
}
/* ------------------------------------------------------------------ */

int main(int argc, char *argv[])
{
int		foo, idx;

if (1 == argc) {
	foo = check_a_file_by_name(NULL);
	}
else	{
	for (idx=1; idx<argc; idx++) {
		printf("%4d %s\n", idx, argv[idx]);
		}
	}

return 0;
}

/* ------------------------------------------------------------------ */
