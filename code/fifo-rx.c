/***		named pipe --- receiver		***/

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <sys/stat.h>		/* for mkfifo */
#include  <fcntl.h>
#include  "my-fifo.h"

int main(int argc, char *argv[])
{
int		fifo;
Message		message;
double		localTS;

if (argc!=2) {
	fprintf(stderr, "%s need a fifo name\n", argv[0]);
	exit(1);
	}
fifo = mkfifo(argv[1], O_RDONLY|O_CREAT);
if (-1==fifo) {
	perror("mkfifo fail");
	exit(2);
	}
read(fifo, &message, sizeof(Message));
localTS = dtime();
printf("%f  %f\n", localTS, message.timestamp);
close(fifo);
return 0;
}