/*               get-signal.c             */

#include  <stdio.h>
#include  <unistd.h>
#include  <signal.h>

volatile int drapeau;

void attraper(int value)
{
drapeau = 1;
}

int main(int argc, char *argv[])
{
int		foo;

printf("kill me, my pid is %d\n", getpid());
signal(SIGUSR1, attraper);

for (foo=0; foo<1337; foo++) {
	if (drapeau) {
		printf("count is %d\n", foo);
		drapeau = 0;
		}
	sleep(1);	/* simulate heavy computing */
	}
return 0;
}

