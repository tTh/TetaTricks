/* HELLO.POV */
#version 3.7;
global_settings { assumed_gamma 1.0 }
#include "colors.inc"

plane          { y, 0 }
sphere         { <0, 0.50, 0>, 0.333 }
light_source   { <-210, 350, -530>  color rgb <1, 1, 1>}
