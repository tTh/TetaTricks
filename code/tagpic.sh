#!/bin/bash

function tagpic
{
infile="$1"
outfile="$2"
texte="$3"
txtfont=" -font Utopia "
fontsize=" -pointsize 96 -kerning 4 "
colors=" -fill Gray20 -stroke White "
txtopts=" -antialias -alpha off $txtfont "

convert	        $infile	                           \
                ${txtopts}                         \
                ${txtfont} ${fontsize}             \
                ${colors}                           \
                -gravity South                     \
                -annotate +0+85 "${texte}"         \
                $outfile
}

if [ "$#" -eq 1 ] ; then
	tagpic foo.png bar.png "$1"
	display bar.png
fi


