/*
 *  spy_getenv.so:	spy_getenv.c Makefile
 *  	gcc -Wall -shared -fPIC  $< -ldl -o $@
 */
#include  <stdio.h>
#include  <string.h>
#include  <unistd.h>
#define   __USE_GNU
#include  <dlfcn.h>

typedef char * (*original_getenv)(const char *envname);

char *getenv(char *envname)
{
static char		*arrow = "--getenv-->  ";
static char		*wtf = " --> (nil)";
char			*content;
original_getenv		orig_getenv;

orig_getenv = (original_getenv)dlsym(RTLD_NEXT, "getenv");
write(STDERR_FILENO, arrow, strlen(arrow));
write(STDERR_FILENO, envname, strlen(envname));
content = orig_getenv(envname);
if (NULL != content) {
	write(STDERR_FILENO, "=", 1);
	write(STDERR_FILENO, content, strlen(content));
	}
else	{
	write(STDERR_FILENO, wtf, strlen(wtf));
	}
write(STDERR_FILENO, "\n", 1);
return content;
}

/* j'adore quand un plan se deroule sans accroc */

