#!/bin/bash

TITLE="Here Doc ($$)"

gnuplot << __EOC__
  set term dumb size 72,24
  # set grid
  set title "$TITLE"
  plot [x=0:pi*3] [-1:1] 0.7*sin(x) 
__EOC__