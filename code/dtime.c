#include <stddef.h>
#include <sys/time.h>
#include "my-fifo.h"
double dtime(void)
{
struct timeval tv;
gettimeofday(&tv, NULL);
return (double)tv.tv_sec + ((double)tv.tv_usec)/1e6;
}