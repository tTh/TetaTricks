#!/usr/bin/awk -f

# create the sdl object: an union of sphere
BEGIN	{
	nbre = 0
        print "#declare Bubulles = object"
	print "{ \nunion  { "
	}

# active portion of the code
	{
	printf("  sphere { <%f, %f, %f>, %f }\n", 	\
			    $1, $2, $3, $4 )
	nbre++
	}

# closing sdl structure
END	{
	print  "  }\n}"
	printf "// %d bubulles\n", nbre
	}
