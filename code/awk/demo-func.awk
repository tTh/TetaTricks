#!/usr/bin/awk -f
BEGIN	{
	print "---- demo fonction ----"
	print_sinus(1.0)
	print_dist(0.5, 0.5, 0.0)
	}

# the simple version
function print_sinus ( value )
{
printf "sin(%f) = %f\n", value, sin(value)
}

# version with local variables.
function print_dist(x, y, z,    dist)
{
dist = sqrt(x*x + y*y + z*z)
printf "distance from center = %f\n", dist
}

