#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
int main(int argc, char *argv[])
{
int		key;

if (2 != argc) {
	fputs("need an argument\n", stderr);
	exit(1);
	}
initscr();		/* first initialization */
cbreak();		/* no line buffering */
noecho();		/* be silent on input */
keypad(stdscr, TRUE);	/* acces touches curseur */

mvaddstr(10, 3, argv[1]);
refresh();
key = getch();
endwin();
printf("code touche  %d  0x%X\n", key, key);

return 0;
}


