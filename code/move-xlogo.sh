#!/bin/bash

xlogo &
sleep 1
job=$!    ;   echo "job = " $job

WIN=$(xdotool search -name 'xlogo')
echo "win = " $WIN

for foo in $(seq 10 33 900)
do
	sleep 1
	xdotool windowmove $WIN 50 $foo
done

kill $job
