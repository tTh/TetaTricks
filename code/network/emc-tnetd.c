/*
 *		EMC SRVTELNET
 */

#include  <stdio.h>
#include  <unistd.h>
#include  <stdlib.h>
#include  <string.h>
#include  <sys/socket.h>
#include  <netinet/in.h>
#include  <arpa/inet.h>

#define BINDADDR	"127.0.0.1"
#define BINDPORT	5678

#define TL		20		/* important value */

int main(int argc, char *argv[])
{
int			foo, len, sock;
char			*adr_ip  = BINDADDR;
int			port     = BINDPORT;
unsigned int		addrlen;
int			connected;
struct sockaddr_in	addr;
struct in_addr		ia;
FILE			*fpin;
char			line[TL+5];

fprintf(stderr, "--- %s ---\n", argv[0]);

sock = socket(AF_INET, SOCK_STREAM, 0);
if (sock < 0) {
	perror(__FILE__ " start socket");
	return -1;
	}

#define S sizeof(struct sockaddr_in)
memset(&addr, 0, S); 
addr.sin_family = AF_INET;
addr.sin_port = htons(port);
if (adr_ip && inet_aton(adr_ip, &ia)) {
	addr.sin_addr = ia;
	fprintf(stderr, "attached to %s\n", adr_ip);
	}
if ((foo=bind(sock, (struct sockaddr *)&addr, S)) < 0) {
	perror(__FILE__ " fail on bind ");
	close(sock);
	return -2;
	}
fprintf(stderr, "bind -> %d\n", foo);

foo = listen(sock, 5);		/* 5 is a bsd magic number */
if (foo) {
	perror(__FILE__ " no magic in listen ");
	return -3;
	}

/* start of the endless loop */
for (;;) {
	fprintf(stderr, "accepting...\n");
	addrlen = sizeof(struct sockaddr);
	connected = accept(sock, (struct sockaddr *)&addr, &addrlen);
	if (connected < 0) {
		perror(__FILE__ " accept");
		return -1;
		}
	fprintf(stderr, "accept -> %d\n", connected);

	if (NULL==(fpin = fdopen(connected, "r+"))) {
		perror(__FILE__ " fdopen on connected");
		return -3;
		}
	// fprintf(stderr, "fopened...\n");

	while ( fgets(line, TL, fpin) ) {
		len = strlen(line);
		fprintf(stderr, "   %3d  |  %s", len, line);
		for (foo=0; foo<len; foo++) {
			fprintf(fpin, "%02X ", (unsigned char)line[foo]);
			}
		fputs("\n", fpin);
		}
	fclose(fpin);
	// close(connected);
	} /* end of the endless loop */

return 0;
}
