# Network / Réseau

Some experiments on network programming.

## Telnet server

Une étude pour pouvoir intégrer un accès "console" à n'importe quelle
application. La première ébauche a rapidement permis de comprendre
que c'est pas si simple que ça.

Le fichier qui permet de mettre en évidence les soucis est assez
simple pour être facile à suivre : `emc-tnetd.c`.

### Le buffer-overflow.

Enfin pas vraiment un dépassement de tampon, mais juste
une inconpréhension sur la sémantique de `fgets` sur les conditions
d'erreur. Et dans un contexte particulier : le FILE * est fourni
par un `fdopen` appliqué sur un socket TCP, mais je ne sais pas
encore si ça a une réelle influence.

### Les caractères de controle.

Stay tuned, film at 11.



